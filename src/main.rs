use iced::{ text_input, button, Sandbox, Element, Length, Column,
    Text, Container, HorizontalAlignment, Settings, TextInput,
    Button, Row, scrollable, Scrollable };

mod message;
use message::*;

#[derive(Default)]
struct IcedTextApp {
    input: text_input::State,
    submit_btn: button::State,
    scroll: scrollable::State,
    input_value: String,
    result_value: String
}

impl Sandbox for IcedTextApp {
    type Message = Message;

    fn new() -> Self {
        return Self::default();
    }

    fn title(&self) -> String {
        "Iced test app with textbox".to_string()
    }

    fn update(&mut self, msg: Message) -> () {
        match msg {
            Message::InputChanged(value) => {
                self.input_value = value;
            }
            Message::SubmitButtonPressed => {
                self.result_value = get_result_text();
            }
        }
    }

    fn view(&mut self) -> Element<Message> {
        let title = Text::new("Title")
        .width(Length::Fill)
        .size(100)
        .color([0.5, 0.5, 0.5])
        .horizontal_alignment(HorizontalAlignment::Center);

        let input = TextInput::new(
            &mut self.input,
            "Input stuff",
            &mut self.input_value,
            Message::InputChanged,
        )
        .padding(15)
        .size(30);

        let send_btn = Button::new(&mut self.submit_btn, Text::new("Submit"))
            .on_press(Message::SubmitButtonPressed);

        let input_row: Element<Message> = Row::new()
            .spacing(20)
            .push(input)
            .push(send_btn)
            .into();

        let result_text = Text::new(&self.result_value);

        let scr: Element<Message> = Scrollable::new(&mut self.scroll)
            .padding(40)
            .push(result_text)
            .into();

        let content = Column::new()
            .max_width(800)
            .spacing(20)
            .push(title)
            .push(input_row)
            .push(scr);            

        Container::new(content)
        .width(Length::Fill)
        .height(Length::Fill)
        .center_x()
        .center_y()
        .into()
    }
}

fn get_result_text() -> String {
    let mut res = String::new();

    for _ in 1..1000{
        res += "Lorem ipsum ";
    }

    return res;
}

fn main() {
    let _ = IcedTextApp::run(Settings::default());
}